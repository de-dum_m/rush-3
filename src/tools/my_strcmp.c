/*
** my_strcmp.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 05:38:46 2014 chapui_s
** Last update Sat May 10 05:38:47 2014 chapui_s
*/

int		my_strcmp(char *s1, char *s2)
{
  return ((*s1 == *s2 && *s1) ? (my_strcmp(++s1, ++s2)) : (*s1 - *s2));
}
