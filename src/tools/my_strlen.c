/*
** my_strlen.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 01:18:57 2014 chapui_s
** Last update Sat May 10 01:19:36 2014 chapui_s
*/

int		my_strlen(char *s)
{
  int		size;

  size = 0;
  while (s && s[size])
    size += 1;
  return (size);
}
