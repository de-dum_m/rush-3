/*
** my_atoi.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 01:39:31 2014 chapui_s
** Last update Sun May 11 18:00:09 2014 menigo_m
*/

unsigned int	my_uatoi(char *str)
{
  int			i;
  unsigned int		number;

  i = 0;
  number = 0;
  while (str && str[i] && str[i] >= 48 && str[i] <= 57)
    {
      number *= 10;
      number += ((int)str[i] - 48);
      i++;
    }
  return (number);
}

int	my_atoi(char *str)
{
  int	i;
  int	number;

  i = 0;
  number = 0;
  while (str && str[i] && str[i] >= 48 && str[i] <= 57)
    {
      number *= 10;
      number += ((int)str[i] - 48);
      i++;
    }
  return (number);
}
