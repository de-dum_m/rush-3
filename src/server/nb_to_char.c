/*
** nb_to_char.c for  in /home/lopes_n/rendu/rush-3
** 
** Made by lopes_n
** Login   <lopes_n@epitech.net>
** 
** Started on  Sun May 11 14:37:37 2014 lopes_n
** Last update Sun May 11 14:37:42 2014 lopes_n
*/

#include <stdlib.h>

char	*nb_to_char(int nb)
{
  char	*out;
  int	size;
  int	pow;
  int	i;

  size = 0;
  pow = 1;
  i = 0;
  while (pow <= nb / 10)
    {
      ++size;
      pow *= 10;
    }
  if ((out = malloc(sizeof(char) * (size + 2))) == NULL)
    return (NULL);
  while (i < size + 1)
    {
      out[i] = nb / pow + '0';
      nb = nb % pow;
      pow = pow / 10;
      i = i + 1;
    }
  out[i] = '\0';
  return (out);
}
