/*
** fill_header.c for  in /home/dong_n/rendu/rush-3/src/server
**
** Made by dong_n
** Login   <dong_n@epitech.net>
**
** Started on  Sat May 10 20:18:34 2014 dong_n
** Last update Sun May 11 17:29:14 2014 menigo_m
*/

#include <unistd.h>
#include <stdlib.h>
#include "server/server.h"

static void	*get_header_elem(char value, t_editor *datas)
{
  int		i;
  t_attr	attr[3];

  attr[0].code = NAME;
  attr[0].name = &(datas->game_name);
  attr[1].code = ROOM_TO_START;
  attr[1].name = &(datas->start);
  attr[2].code = ROOM_TO_WIN;
  attr[2].name = &(datas->end);
  i = 0;
  while (i < 3)
    {
      if (attr[i].code == value)
	return (attr[i].name);
      i++;
    }
  my_put_error("Invalid header element\n");
  return (NULL);
}

int		fill_header(t_editor *datas, int fd)
{
  char		value;
  char		**ptr;
  int		rd;

  if ((rd = read(fd, &value, 1)) < 0)
    return (my_put_error("Read failed\n"));
  while (value != SEP_SECTION)
    {
      if (!(ptr = get_header_elem(value, datas)))
	return (-1);
      if (!(*ptr = get_value(fd)))
	return (-1);
      if ((rd = read(fd, &value, 1)) < 0)
	return (my_put_error("Read failed\n"));
    }
  return (0);
}
