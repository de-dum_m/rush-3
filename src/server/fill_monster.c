/*
** fill_monster.c for  in /home/dong_n/rendu/rush-3/src/server
**
** Made by dong_n
** Login   <dong_n@epitech.net>
**
** Started on  Sat May 10 20:18:18 2014 dong_n
** Last update Sat May 10 20:18:18 2014 dong_n
*/

#include <unistd.h>
#include <stdlib.h>
#include "server/server.h"

static void	init_attr_monster(t_attr *attr, t_monster *monster)
{
  attr[0].code = TYPE;
  attr[0].name = &(monster->type);
  attr[1].code = WEAPON;
  attr[1].name = &(monster->weapon);
  attr[2].code = HP;
  attr[2].name = &(monster->hp);
  attr[3].code = SPE;
  attr[3].name = &(monster->spe);
  attr[4].code = SPEED;
  attr[4].name = &(monster->speed);
  attr[5].code = DEG;
  attr[5].name = &(monster->deg);
  attr[6].code = ARMOR;
  attr[6].name = &(monster->armor);
}

static void	*get_monster_elem(char value, t_monster *monster, int *i)
{
  t_attr	attr[8];

  init_attr_monster(attr, monster);
  while (*i < 7)
    {
      if (attr[*i].code == value)
	return (attr[*i].name);
      ++(*i);
    }
  my_put_error("Invalid attribute for monster\n");
  *i = -1;
  return (NULL);
}

static t_monster	*new_monster(t_monster *monster)
{
  t_monster		*elem;
  t_monster		*tmp;
  static int		i = 0;

  if (!(elem = calloc(1, sizeof(*elem))))
    return (NULL);
  elem->id = i++;
  elem->next = NULL;
  tmp = monster;
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  return (elem);
}

static int	fill_monster_elem(char value, t_monster *monster, int fd)
{
  int		i;
  char		*str;
  char		**str_ptr;
  void		*ptr;

  i = 0;
  if (!(ptr = get_monster_elem(value, monster, &i)))
    return (-1);
  if (i == 0 || i == 1)
    {
      if (!(str = get_value(fd)))
	return (-1);
      str_ptr = ptr;
      *str_ptr = str;
    }
  else if (i > 1 && get_nbr_value(ptr, fd) == -1)
    return (-1);
  return (0);
}

int		fill_monster(t_editor *datas, int fd)
{
  char		value;
  int		rd;
  t_monster	*monster;

  if (!(monster = new_monster(datas->monsters)))
    return (my_put_error("Failed to create new monster\n"));
  if ((rd = read(fd, &value, 1)) < 0)
    return (my_put_error("Read failed\n"));
  while (value != SEP_SECTION)
    {
      if (fill_monster_elem(value, monster, fd) == -1)
	return (-1);
      if ((rd = read(fd, &value, 1)) < 0)
	return (my_put_error("Read failed\n"));
    }
  return (0);
}
