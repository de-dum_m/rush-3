/*
** fill_room.c for  in /home/lopes_n/rendu/rush-3/src/server
**
** Made by lopes_n
** Login   <lopes_n@epitech.net>
**
** Started on  Sat May 10 20:30:54 2014 lopes_n
** Last update Sun May 11 09:41:08 2014 dong_n
*/

#include <unistd.h>
#include <stdlib.h>
#include "server/server.h"

static void	init_attr_room(t_attr *attr, t_room *room)
{
  attr[0].code = NAME;
  attr[0].name = &(room->name);
  attr[1].code = ADV;
  attr[1].name = &(room->adv);
  attr[2].code = CONNECTION_TAB;
  attr[2].name = &(room->connections);
  attr[3].code = MONSTER_TAB;
  attr[3].name = &(room->monster_id);
}

static void	*get_room_elem(char value, t_room *room, int *i)
{
  t_attr	attr[4];

  init_attr_room(attr, room);
  while (*i < 4)
    {
      if (attr[*i].code == value)
	return (attr[*i].name);
      ++(*i);
    }
  my_put_error("Invalid attribute for room\n");
  *i = -1;
  return (NULL);
}

static t_room	*new_room(t_room *room)
{
  t_room	*elem;
  t_room	*tmp;

  if (!(elem = calloc(1, sizeof(*elem))) ||
      !(elem->monster_id = calloc(1, sizeof(*(elem->monster_id)))) ||
      !(elem->connections = calloc(1, sizeof(*(elem->connections)))))
    return (NULL);
  elem->nb_connections = 0;
  elem->nb_monsters = 0;
  elem->next = NULL;
  tmp = room;
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  return (elem);
}

static int	fill_room_elem(char value, t_editor *datas, int fd, t_room *room)
{
  int		i;
  char		*str;
  char		**str_ptr;
  void		*ptr;

  i = 0;
  if (!(ptr = get_room_elem(value, room, &i)))
    return (-1);
  if (!(str = get_value(fd)))
    return (-1);
  if (i < 2)
    {
      str_ptr = ptr;
      *str_ptr = str;
    }
  else if (i == 2 && fill_room_connections(datas, str, room) == -1)
    return (-1);
  else if (i == 3 && fill_room_monster(datas, str, room) == -1)
    return (-1);
  return (0);
}

int		fill_room(t_editor *datas, int fd)
{
  char		value;
  int		rd;
  t_room	*room;

  if (!(room = new_room(datas->rooms)))
    return (my_put_error("Failed to create new room\n"));
  if ((rd = read(fd, &value, 1)) < 0)
    return (my_put_error("Read failed\n"));
  while (value != SEP_SECTION)
    {
      if (fill_room_elem(value, datas, fd, room) == -1)
	return (-1);
      if ((rd = read(fd, &value, 1)) < 0)
	return (my_put_error("Read failed\n"));
    }
  return (0);
}
