/*
** fill_champion.c for  in /home/dong_n/rendu/rush-3/src/server
**
** Made by dong_n
** Login   <dong_n@epitech.net>
**
** Started on  Sat May 10 20:18:23 2014 dong_n
** Last update Sun May 11 16:04:37 2014 lopes_n
*/

#include <unistd.h>
#include <stdlib.h>
#include "server/server.h"

static void	init_attr_champ(t_attr *attr, t_champ *champs)
{
  attr[0].code = NAME;
  attr[0].name = &(champs->name);
  attr[1].code = TYPE;
  attr[1].name = &(champs->type);
  attr[2].code = WEAPON;
  attr[2].name = &(champs->weapon);
  attr[3].code = HP;
  attr[3].name = &(champs->hp);
  attr[4].code = SPE;
  attr[4].name = &(champs->spe);
  attr[5].code = SPEED;
  attr[5].name = &(champs->speed);
  attr[6].code = DEG;
  attr[6].name = &(champs->deg);
  attr[7].code = ARMOR;
  attr[7].name = &(champs->armor);
}

static void	*get_champion_elem(char value, t_champ *champs, int *i)
{
  t_attr	attr[8];

  init_attr_champ(attr, champs);
  while (*i < 8)
    {
      if (attr[*i].code == value)
	return (attr[*i].name);
      ++(*i);
    }
  my_put_error("Invalid attribute for champion\n");
  *i = -1;
  return (NULL);
}

static t_champ	*new_champ(t_champ *champ)
{
  t_champ	*elem;
  t_champ	*tmp;

  if (!(elem = calloc(1, sizeof(*elem))))
    return (NULL);
  elem->next = NULL;
  tmp = champ;
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  return (elem);
}

static int	fill_champion_elem(char value, t_champ *champs, int fd)
{
  int		i;
  char		*str;
  char		**str_ptr;
  void		*ptr;

  i = 0;
  if (!(ptr = get_champion_elem(value, champs, &i)))
    return (-1);
  if (i >= 0 && i < 3)
    {
      if (!(str = get_value(fd)))
	return (-1);
      str_ptr = ptr;
      *str_ptr = str;
    }
  else if (i > 2 && get_nbr_value(ptr, fd) == -1)
    return (-1);
  return (0);
}

int		fill_champion(t_editor *datas, int fd)
{
  char		value;
  int		rd;
  t_champ	*champs;

  if (!(champs = new_champ(datas->champs)))
    return (my_put_error("Failed to create new champ\n"));
  if ((rd = read(fd, &value, 1)) < 0)
    return (my_put_error("Read failed\n"));
  while (value != SEP_SECTION)
    {
      if (fill_champion_elem(value, champs, fd) == -1)
	return (-1);
      if ((rd = read(fd, &value, 1)) < 0)
	return (my_put_error("Read failed\n"));
    }
  return (0);
}
