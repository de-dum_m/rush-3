/*
** list_monster.c for  in /home/lopes_n/rendu/rush-3
** 
** Made by lopes_n
** Login   <lopes_n@epitech.net>
** 
** Started on  Sun May 11 14:36:10 2014 lopes_n
** Last update Sun May 11 17:28:44 2014 menigo_m
*/

#include <string.h>
#include <stdlib.h>
#include "server/server.h"

static char		*init_str_monster(char *str, int value)
{
  str = strcpy(str, "OK NB:");
  str = strcat(str, nb_to_char(value));
  str = strcat(str, " ");
  return (str);
}

static char		*complete_str_monster(char *str, t_monster *tmp)
{
  str = strcat(str, "MONSTER:");
  str = strcat(str, "TYPE=");
  str = strcat(str, tmp->type);
  str = strcat(str, ",HP=");
  str = strcat(str, nb_to_char(tmp->hp));
  str = strcat(str, ",SPE=");
  str = strcat(str, nb_to_char(tmp->spe));
  str = strcat(str, ",SPEED=");
  str = strcat(str, nb_to_char(tmp->speed));
  str = strcat(str, ",DEG=");
  str = strcat(str, nb_to_char(tmp->deg));
  str = strcat(str, ",WEAPON=");
  str = strcat(str, tmp->weapon);
  str = strcat(str, ",ARMOR=");
  str = strcat(str, nb_to_char(tmp->armor));
  str = strcat(str, " ");
  return (str);
}

char		*list_monster(t_editor *datas, int value)
{
  t_monster	*tmp;
  char		*str;
  int		size;

  tmp = datas->monsters->next;
  size = 7 + my_strlen(nb_to_char(value));
  while (tmp != NULL)
    {
      size += 55 + my_strlen(tmp->type) + my_strlen(nb_to_char(tmp->hp))
	+ my_strlen(nb_to_char(tmp->spe)) + my_strlen(nb_to_char(tmp->speed))
	+ my_strlen(nb_to_char(tmp->deg)) + my_strlen(tmp->weapon)
	+ my_strlen(nb_to_char(tmp->armor));
      tmp = tmp->next;
    }
  if ((str = malloc(sizeof(char) * (size + 1))) == NULL)
    return (NULL);
  str = init_str_monster(str, value);
  tmp = datas->monsters->next;
  while (tmp != NULL)
    {
      str = complete_str_monster(str, tmp);
      tmp = tmp->next;
    }
  return (str);
}
