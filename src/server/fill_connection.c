/*
** fill_connection.c for  in /home/dong_n/rendu/rush-3/src/server
**
** Made by dong_n
** Login   <dong_n@epitech.net>
**
** Started on  Sun May 11 09:16:27 2014 dong_n
** Last update Sun May 11 17:28:23 2014 menigo_m
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "server/server.h"

static int	check_conect_name(t_room *room, char *name)
{
  while (room != NULL)
    {
      if (strcmp(room->name, name) == 0)
	return (0);
      room = room->next;
    }
  return (-1);
}

int		exist_connection(t_editor *datas, t_room *tmp)
{
  int		i;

  if (tmp == NULL)
    return (0);
  i = 0;
  while (i < tmp->nb_connections)
    {
      if (check_conect_name(datas->rooms->next, tmp->connections[i]) == -1)
	return (-1);
      i = i + 1;
    }
  return (exist_connection(datas, tmp->next));
}

static int	add_connection_to_room(t_room *room, char *name)
{
  ++(room->nb_connections);
  room->connections = realloc(room->connections,
			      room->nb_connections *
			      sizeof(*(room->connections)));
  if (room->connections == NULL)
    return (my_put_error("Realloc failed\n"));
  room->connections[room->nb_connections - 1] = name;
  return (0);
}

int	fill_room_connections(t_editor *datas, char *connection, t_room *room)
{
  int	i;
  int	size;
  char	*name;

  i = 0;
  size = my_elem_len(connection);
  if (!(name = malloc(sizeof(char) * (size + 1))))
    return (my_put_error("Malloc failed\n"));
  while (connection[i] && connection[i] != '=')
    {
      name[i] = connection[i];
      ++i;
    }
  name[i] = '\0';
  if (add_connection_to_room(room, name) == -1)
    return (-1);
  if (connection[i++] == '=')
    return (fill_room_connections(datas, &(connection[i]), room));
  return (0);
}
