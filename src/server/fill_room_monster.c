/*
** fill_room_monster.c for  in /home/dong_n/rendu/rush-3/src/server
**
** Made by dong_n
** Login   <dong_n@epitech.net>
**
** Started on  Sat May 10 20:55:29 2014 dong_n
** Last update Sun May 11 14:39:51 2014 lopes_n
*/

#include <string.h>
#include <stdlib.h>
#include "server/server.h"

int		my_elem_len(char *str)
{
  int		i;

  i = 0;
  while (str[i] && str[i] != '=')
    i++;
  return (i);
}

static int	exist_monster(char *name, t_editor *datas)
{
  t_monster	*tmp;

  tmp = datas->monsters->next;
  while (tmp != NULL)
    {
      if (strcmp(name, tmp->type) == 0)
	return (tmp->id);
      tmp = tmp->next;
    }
  return (-1);
}

static int	add_monster_to_room(t_editor *datas, t_room *room, char *name)
{
  int		id_monster;

  if ((id_monster = exist_monster(name, datas)) == -1)
    return (my_put_error("Monster does not exist\n"));
  ++(room->nb_monsters);
  room->monster_id = realloc(room->monster_id,
			     room->nb_monsters *
			     sizeof(*(room->monster_id)));
  if (room->monster_id == NULL)
    return (my_put_error("Realloc failed\n"));
  room->monster_id[room->nb_monsters - 1] = id_monster;
  return (0);
}

int		fill_room_monster(t_editor *datas, char *monster, t_room *room)
{
  int		i;
  int		size;
  char		*name;

  i = 0;
  size = my_elem_len(monster);
  if (!(name = malloc(sizeof(char) * (size + 1))))
    return (my_put_error("Malloc failed\n"));
  while (monster[i] && monster[i] != '=')
    {
      name[i] = monster[i];
      ++i;
    }
  name[i] = '\0';
  if (add_monster_to_room(datas, room, name) == -1)
    return (-1);
  if (monster[i++] == '=')
    return (fill_room_monster(datas, &(monster[i]), room));
  return (0);
}
