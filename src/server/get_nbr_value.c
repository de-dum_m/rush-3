/*
** get_nbr_value.c for  in /home/lopes_n/rendu/rush-3/src/server
** 
** Made by lopes_n
** Login   <lopes_n@epitech.net>
** 
** Started on  Sun May 11 15:12:10 2014 lopes_n
** Last update Sun May 11 17:27:55 2014 menigo_m
*/

#include <unistd.h>
#include <stdlib.h>
#include "server/server.h"

char	*get_value(int fd)
{
  char	size;
  char	*str;
  int	rd;

  if ((rd = read(fd, &size, 1)) < 0 ||
      !(str = malloc(sizeof(*str) * (size + 1))) ||
      (rd = read(fd, str, size)) < 0)
    {
      my_put_error("Failed to get value\n");
      return (NULL);
    }
  str[rd] = '\0';
  return (str);
}

int	get_nbr_value(void *ptr, int fd)
{
  char	*str;
  int	nbr;
  int	*nbr_ptr;

  if (!(str = get_value(fd)))
    return (-1);
  nbr = atoi(str);
  nbr_ptr = ptr;
  *nbr_ptr = nbr;
  return (0);
}
