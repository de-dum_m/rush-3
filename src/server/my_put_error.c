/*
** my_put_error.c for  in /home/lopes_n/rendu/rush-3/src/server
** 
** Made by lopes_n
** Login   <lopes_n@epitech.net>
** 
** Started on  Sun May 11 15:09:56 2014 lopes_n
** Last update Sun May 11 15:15:03 2014 lopes_n
*/

#include <unistd.h>
#include "tools/tools.h"

int		my_put_error(char *str)
{
  write(2, "\033[31m", 5);
  write(2, str, my_strlen(str));
  write(2, "\033[0m", 4);
  return (-1);
}
