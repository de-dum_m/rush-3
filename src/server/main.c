/*
** main.c for  in /home/menigo_m/rendu/rush-3/src/server
**
** Made by menigo_m
** Login   <menigo_m@epitech.net>
**
** Started on  Sat May 10 20:18:12 2014 menigo_m
** Last update Sun May 11 17:26:25 2014 menigo_m
*/

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "server/server.h"

static const t_section	g_section[] =
  {
    {CHAMPION, &fill_champion},
    {HEADER, &fill_header},
    {MONSTER, &fill_monster},
    {ROOM, &fill_room}
  };

static int	get_section(char value, t_editor *datas, int fd)
{
  int		i;

  i = 0;
  while (i < 4)
    {
      if (value == g_section[i].name)
	return (g_section[i].func(datas, fd));
      i++;
    }
  my_put_error("Invalid section\n");
  return (-1);
}

static int	is_multiple(t_champ *champs)
{
  t_champ	*tmp;

  tmp = champs;
  while (tmp != NULL)
    {
      if (strcmp(tmp->name, champs->name) == 0)
	return (1);
      tmp = tmp->next;
    }
  return (0);
}

static int	check_champions(t_champ *champs)
{
  t_champ	*tmp;

  tmp = champs;
  while (tmp != NULL)
    {
      if (is_multiple(tmp) == 1)
	return (-1);
      tmp = tmp->next;
    }
  return (0);
}

static int	fill_datas(t_editor *datas, int fd)
{
  char		value;
  int		rd;

  if ((rd = read(fd, &value, 1)) < 0)
    return (my_put_error("Read failed\n"));
  if (value != MAGIC_NUMBER)
    return (my_put_error("Invalid Magic Number !\n"));
  while ((rd = read(fd, &value, 1)) > 0)
    {
      if (get_section(value, datas, fd) == -1)
	return (-1);
    }
  if (exist_connection(datas, datas->rooms->next) == -1)
    return (my_put_error("Connection does not exist\n"));
  if (check_champions(datas->champs->next) == -1)
    return (my_put_error("Multiple definition of champ\n"));
  return (0);
}

int		main(int argc, char **argv)
{
  int		fd;
  t_editor      *datas;

  if (argc == 1)
    return (-1);
  if (!(datas = calloc(1, sizeof(*datas))) ||
      !(datas->champs = calloc(1, sizeof(*(datas->champs)))) ||
      !(datas->monsters = calloc(1, sizeof(*(datas->monsters)))) ||
      !(datas->rooms = calloc(1, sizeof(*(datas->rooms)))))
    return (-1);
  if ((fd = open(argv[1], O_RDONLY)) < 0)
    return (-1);
  if (fill_datas(datas, fd) == -1)
    return (-1);
  return (0);
}
