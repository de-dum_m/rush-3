/*
** left_menu.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 11:42:30 2014 de-dum_m
** Last update Sun May 11 14:37:12 2014 de-dum_m
*/

#include <curses.h>
#include "client/client.h"

static int	init_cmd_list(WINDOW *tl, t_client *clt)
{
  int		i;

  i = 1;
  while (i < 7)
    {
      mvwprintw(tl, i + 3, 2, clt->cmds[i]);
      i++;
    }
  wattron(tl, A_REVERSE);
  mvwprintw(tl, 3, 2, clt->cmds[0]);
  wattroff(tl, A_REVERSE);
  wrefresh(tl);
  keypad(tl, TRUE);
  noecho();
  cbreak();
  return (SUCCESS);
}

static int	cmd_list(WINDOW *tl, t_client *clt)
{
  int		cur_pos;
  int		key;

  cur_pos = 0;
  init_cmd_list(tl, clt);
  while ((key = wgetch(tl)) != '\n')
    {
      if (key <= 0 || key == 27)
	return (FAILURE);
      mvwprintw(tl, cur_pos + 3, 2, clt->cmds[cur_pos]);
      if (key == KEY_UP)
	cur_pos--;
      else if (key == KEY_DOWN)
	cur_pos++;
      if (cur_pos > 6)
	cur_pos = 0;
      else if (cur_pos < 0)
	cur_pos = 6;
      wattron(tl, A_REVERSE);
      mvwprintw(tl, cur_pos + 3, 2, clt->cmds[cur_pos]);
      wattroff(tl, A_REVERSE);
      wrefresh(tl);
    }
  clt->cmd = cur_pos;
  return (SUCCESS);
}

int	left_menu(WINDOW *tl, t_client *clt)
{
  if (wattron(tl, A_BOLD) == ERR)
    return (FAILURE);
  wmove(clt->win->bc, 2, 0);
  wclrtobot(clt->win->bc);
  wrefresh(clt->win->bc);
  mvwprintw(tl, 1, 0, "Select command:");
  if (wattroff(tl, A_BOLD) == ERR)
    return (FAILURE);
  if (cmd_list(tl, clt) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}
