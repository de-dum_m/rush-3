/*
** gui.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 00:55:20 2014 de-dum_m
** Last update Sun May 11 15:49:38 2014 de-dum_m
*/

#include <curses.h>
#include <stdlib.h>
#include "client/client.h"
#include "client/gui.h"
#include "tools/tools.h"

int	start(t_client *clt)
{
  char	*str;
  char	*tmp;

  clear();
  echo();
  if (!(clt->addr = malloc(MAX_LEN)))
    return (FAILURE);
  str = "[  Please enter server IP  ]";
  mvprintw(LINES / 2, (COLS  - my_strlen(str)) / 2, "%s\n", str);
  mvprintw(LINES / 2 + 1, (COLS / 2) - 9, "%s", " ");
  refresh();
  if ((getnstr(clt->addr, MAX_LEN - 1) == ERR)
      || (!(tmp = malloc(MAX_LEN))))
    return (FAILURE);
  str = "[ Please enter port number ]";
  move(LINES / 2 + 1, 0);
  clrtoeol();
  mvprintw(LINES / 2, (COLS  - my_strlen(str)) / 2, "%s\n", str);
  mvprintw(LINES / 2 + 1, (COLS / 2) -2, "%s", " ");
  clrtoeol();
  refresh();
  if (getnstr(tmp, MAX_LEN - 1) == ERR)
    return (FAILURE);
  clt->portnum = my_atoi(tmp);
  return (client_connect(clt->addr, clt->portnum, clt));
}

void	usage(WINDOW *tc)
{
  wprintw(tc, " 1) Select a command using the arrow keys and enter\n\
 2) If the selected command requires an argument, you'll be prompted to \
enter it. If you change your mind and want to enter a different command, \
press the up arrow key.\n\
 3) Wait for your turn! You can always leave by pressing ESC.\n\n");
}

void	refresh_allwin(t_win *win)
{
  wrefresh(win->br);
  wrefresh(win->bc);
  wrefresh(win->bl);
  wrefresh(win->tr);
  wrefresh(win->tl);
  wrefresh(win->tc);
  wrefresh(win->ml);
}
