/*
** struct.c for client in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 17:20:26 2014 armita_a
** Last update Sun May 11 18:01:09 2014 armita_a
*/

#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include "unistd.h"
#include "client/client.h"

int		get_struct(t_client *clt)
{
  fd_set	rdfs;

  wmove(clt->win->bc, 2, 0);
  wclrtoeol(clt->win->bc);
  mvwprintw(clt->win->bc, 2, 10, "Get back infos");
  wrefresh(clt->win->bc);
  FD_ZERO(&(rdfs));
  FD_SET(clt->sock_fd, &(rdfs));
  if (select(clt->sock_fd + 1, &(rdfs), NULL, NULL, NULL) == FAILURE)
    return (QUIT);
  if (FD_ISSET(clt->sock_fd, &(rdfs)))
    {
      if (read(clt->sock_fd, clt->game, sizeof(t_editor)) != sizeof(t_editor))
	return (FAILURE);
      return (SUCCESS);
    }
  return (FAILURE);
}

void	display_struct(t_client *clt)
{
}
