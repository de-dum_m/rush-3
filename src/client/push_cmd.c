/*
** push_cmd.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 16:45:46 2014 de-dum_m
** Last update Sun May 11 18:41:24 2014 armita_a
*/

#include <curses.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include "client/client.h"
#include "tools/tools.h"
#include "tools/get_next_line.h"

static void	clear_btwin(t_win *win)
{
  wmove(win->bc, 2, 0);
  wclrtoeol(win->bc);
  wmove(win->bc, 3, 0);
  wclrtoeol(win->bc);
  wclear(win->bl);
  wrefresh(win->bc);
  wrefresh(win->bl);
}

static char	*make_packet(t_client *clt)
{
  char		*str;
  char		*tmp;

  if (clt->arg && !(str = malloc(my_strlen(clt->arg) + 3)))
    return (NULL);
  else if (!(clt->arg) && !(str = malloc(2)))
    return (NULL);
  str[0] = clt->cmd + 48;
  str[1] = '\0';
  if (clt->arg)
    {
      tmp = str + 2;
      tmp = strcpy(tmp, clt->arg);
    }
  return (str);
}

int		push_cmd(t_client *clt)
{
  char		*str;

  clear_btwin(clt->win);
  if ((str = make_packet(clt))
      && send(clt->sock_fd, str,
	      my_strlen(clt->arg) + 2, MSG_NOSIGNAL) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}
