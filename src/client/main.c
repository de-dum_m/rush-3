/*
** main.c for server in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/src/server
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri May  9 22:04:08 2014 armita_a
** Last update Sun May 11 18:41:35 2014 armita_a
*/

#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include "client/client.h"
#include "client/gui.h"
#include "tools/tools.h"
#include "tools/get_next_line.h"

static void	fill_clt_cmds(t_client *clt)
{
  clt->cmds[0] = JS_NNEXT;
  clt->cmds[1] = JS_NLIST_T;
  clt->cmds[2] = JS_NLIST_M;
  clt->cmds[3] = JS_NATTACK;
  clt->cmds[4] = JS_NATTACK_SPE;
  clt->cmds[5] = JS_NWHO;
  clt->cmds[6] = JS_NNOEL;
  clt->cmd_arg[0] = "name room";
  clt->cmd_arg[1] = NULL;
  clt->cmd_arg[2] = NULL;
  clt->cmd_arg[3] = "monster id";
  clt->cmd_arg[4] = "monster id";
  clt->cmd_arg[5] = NULL;
  clt->cmd_arg[6] = NULL;
}

static int	wait_for_turn(t_client *clt)
{
  char		*s;
  fd_set	rdfs;

  mvwprintw(clt->win->bc, 2, 10, "Waiting for your turn");
  wrefresh(clt->win->bc);
  FD_ZERO(&(rdfs));
  FD_SET(clt->sock_fd, &(rdfs));
  if (select(clt->sock_fd + 1, &(rdfs), NULL, NULL, NULL) == FAILURE)
    return (QUIT);
  if (FD_ISSET(clt->sock_fd, &(rdfs)))
    {
      if (!(s = get_next_line(clt->sock_fd)))
	return (QUIT);
      wprintw(clt->win->tc, "%s\n", s);
      wrefresh(clt->win->tc);
      if (my_strlen(s) > 5 && strcmp(s + (my_strlen(s) - 4), "TURN") == 0)
	{
	  free(s);
	  return (SUCCESS);
	}
      free(s);
    }
  return (FAILURE);
}

static int	get_loop(t_client *clt)
{
  int		lm;
  int		gc;

  clt->arg = NULL;
  while ((lm = left_menu(clt->win->tl, clt)) != FAILURE
	 && (gc = get_cmd(clt)) != FAILURE)
    {
      if (gc == SUCCESS)
	return (SUCCESS);
      else if (gc == QUIT)
	return (FAILURE);
    }
  return (FAILURE);
}

static int	game_loop(t_client *clt)
{
  int		nb;
  int		gl;

  while (1)
    {
      while ((nb = wait_for_turn(clt)) == FAILURE)
	{
	  if (nb == QUIT)
	    return (FAILURE);
	}
      //      if (get_struct(clt) == FAILURE)
      //return (FAILURE);
      //display_struct(clt);
      if ((gl = get_loop(clt)) == SUCCESS)
	{
	  if (push_cmd(clt) == FAILURE)
	    return (FAILURE);
	}
      else if (gl == FAILURE)
	return (FAILURE);
    }
  return (SUCCESS);
}

int		main(void)
{
  t_win		win;
  t_client	clt;
  char		tmp;

  tmp = 127;
  signal(SIGPIPE, SIG_IGN);
  if (init_gui(&win, &clt) == FAILURE)
    {
      endwin();
      return (FAILURE);
    }
  fill_clt_cmds(&clt);
  clt.win = &win;
  init_center_log(&win);
  usage(win.tc);
  refresh_allwin(&win);
  game_loop(&clt);
  write(clt.sock_fd, &tmp, 1);
  close(clt.sock_fd);
  endwin();
  return (0);
}
