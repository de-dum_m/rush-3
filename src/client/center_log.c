/*
** center_log.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 16:10:21 2014 de-dum_m
** Last update Sat May 10 22:27:57 2014 de-dum_m
*/

#include <curses.h>
#include "client/client.h"

void	init_center_log(t_win *win)
{
  wmove(win->tc, 0, 0);
  scrollok(win->tc, TRUE);
}
