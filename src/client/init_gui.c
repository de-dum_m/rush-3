/*
** init_gui.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 17:05:16 2014 de-dum_m
** Last update Sun May 11 00:10:03 2014 de-dum_m
*/

#include <curses.h>
#include "client/client.h"
#include "client/gui.h"

static int	init_win(t_win *win)
{
  if (!(win->bl = newwin(BAR_SIZE, COL_SIZE, LINES - BAR_SIZE, 0))
      || !(win->bc = newwin(BAR_SIZE + 1, COLS - (COL_SIZE + INFO_SIZE),
			    LINES - BAR_SIZE - 1, COL_SIZE))
      || !(win->br = newwin(LINES / 2, INFO_SIZE, LINES / 2, COLS - INFO_SIZE))
      || !(win->tl = newwin((LINES - BAR_SIZE) / 3 * 2, COL_SIZE, 0, 0))
      || !(win->ml = newwin((LINES - BAR_SIZE) / 3 + 1, COL_SIZE,
			    (LINES - BAR_SIZE) / 3 * 2, 0))
      || !(win->tc = newwin(LINES - BAR_SIZE - 1,
			    COLS - (COL_SIZE + INFO_SIZE), 0, COL_SIZE))
      || !(win->tr = newwin(LINES / 2, INFO_SIZE, 0, COLS - INFO_SIZE)))
    return (FAILURE);
  return (SUCCESS);
}

static void	print_logo(WINDOW *ml_win)
{
  mvwprintw(ml_win, (LINES - BAR_SIZE) / 6 - 3, COL_SIZE / 2 - 6, LOGO0);
  mvwprintw(ml_win, (LINES - BAR_SIZE) / 6 - 2, COL_SIZE / 2 - 6, LOGO1);
  mvwprintw(ml_win, (LINES - BAR_SIZE) / 6 - 1, COL_SIZE / 2 - 6, LOGO2);
  mvwprintw(ml_win, (LINES - BAR_SIZE) / 6, COL_SIZE / 2 - 6, LOGO3);
  mvwprintw(ml_win, (LINES - BAR_SIZE) / 6 + 1, COL_SIZE / 2 - 6, LOGO4);
}

static int	put_borders(t_win *win)
{
  if ((wattron(win->tl, A_REVERSE) == ERR)
      || wattron(win->bc, A_REVERSE) == ERR
      || wattron(win->br, A_REVERSE) == ERR
      || wattron(win->tr, A_REVERSE) == ERR
      || wattron(win->ml, A_REVERSE) == ERR
      || mvwvline(win->tl, 0, COL_SIZE - 1, '|',
		  (LINES - BAR_SIZE) / 3 * 2 - 1) == ERR
      || mvwvline(win->ml, 0, COL_SIZE - 1, '|',
		  (LINES - BAR_SIZE) / 3) == ERR
      || mvwvline(win->tr, 0, 0, '|', LINES / 2) == ERR
      || mvwvline(win->br, 0, 0, '|', LINES / 2) == ERR
      || mvwhline(win->tl, ((LINES - BAR_SIZE) / 3) * 2 - 1, 0,
		  '-', COL_SIZE) == ERR
      || mvwhline(win->ml, (LINES - BAR_SIZE) / 3 - 1, 0, '-', COL_SIZE) == ERR
      || mvwhline(win->bc, 0, 0, '-', COLS - (COL_SIZE * 2)) == ERR
      || mvwhline(win->tr, LINES / 2 - 1, 0, '-', INFO_SIZE) == ERR
      || wattroff(win->tl, A_REVERSE) == ERR
      || wattroff(win->bc, A_REVERSE) == ERR
      || wattroff(win->tr, A_REVERSE) == ERR
      || wattroff(win->ml, A_REVERSE) == ERR
      || wattroff(win->br, A_REVERSE) == ERR)
    return (FAILURE);
  return (SUCCESS);
}

int	init_gui(t_win *win, t_client *clt)
{
  int	nb;

  if (!(initscr()) || size_term() == FAILURE)
    return (FAILURE);
  while ((nb = start(clt)) == FAILURE);
  if (nb == QUIT)
    return (FAILURE);
  if (init_win(win) == FAILURE)
    return (FAILURE);
  print_logo(win->ml);
  if (put_borders(win) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}
