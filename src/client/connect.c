/*
** connect.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 23:28:19 2014 de-dum_m
** Last update Sun May 11 17:20:11 2014 menigo_m
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <curses.h>
#include "tools/tools.h"
#include "client/client.h"

int	size_term(void)
{
  if (COLS < 80 || LINES < 24)
    {
      dprintf(2, "Error : window too small (MIN: 25*80)\n");
      return (FAILURE);
    }
  return (SUCCESS);
}

static int		open_connection(char *addr, int port, t_client *clt)
{
  int			fd;
  struct sockaddr_in	server;

  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return (FAILURE);
  server.sin_addr.s_addr = inet_addr(addr);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  if ((connect(fd, (struct sockaddr*)&server, sizeof(server))) == -1)
    return (FAILURE);
  clt->sock_fd = fd;
  return (SUCCESS);
}

int	client_connect(char *addr, int port, t_client *clt)
{
  int	key;
  char	*str;

  clear();
  str = "Connecting . . .";
  mvprintw(LINES / 2, (COLS  - my_strlen(str)) / 2, "%s", str);
  refresh();
  noecho();
  if (open_connection(addr, port, clt) == FAILURE)
    {
      clear();
      str = "Connection failed, retry? (Y/n): ";
      mvprintw(LINES / 2, (COLS  - my_strlen(str)) / 2, "%s", str);
      refresh();
      while ((key = getch()) != '\n' && key != 'y' && key != 'Y')
	if (key == KEY_ESC || key == 'n' || key == 'N' || key <= 0)
	  return (QUIT);
      return (FAILURE);
    }
  return (SUCCESS);
}
