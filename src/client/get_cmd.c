/*
** get_cmd.c for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 10 13:50:44 2014 de-dum_m
** Last update Sun May 11 16:33:24 2014 de-dum_m
*/

#include <stdlib.h>
#include <unistd.h>
#include <curses.h>
#include "client/gui.h"
#include "client/client.h"
#include "tools/tools.h"

static void	put_cmd_bl(t_client *clt)
{
  wattron(clt->win->bl, A_BOLD);
  mvwprintw(clt->win->bl, 1,
	    (COL_SIZE - my_strlen(clt->cmds[clt->cmd])) / 2 - 2,
	    "%s >", clt->cmds[clt->cmd]);
  wattroff(clt->win->bl, A_BOLD);
  wrefresh(clt->win->bl);
}

static int	special_key(t_client *clt, int key, char *str, int *i)
{
  if (key == 127 && *i > 0)
    {
      str[--(*i)] = '\0';
      wmove(clt->win->bc, 2, *i);
      wclrtoeol(clt->win->bc);
    }
  else if (key == KEY_UP)
    return (RESET);
  else if (key == KEY_ESC || key <= 0)
    return (QUIT);
  else if (key == '\n')
    return (SUCCESS);
  else
    return (FAILURE);
  return (SUCCESS);
}

static int	fill_str(t_client *clt, char *str, int i)
{
  int		nb;
  int		key;

  while (i < 40 && (key = mvwgetch(clt->win->bc, 2, i))
	 && ((i > 0 && key != '\n') || i == 0))
    {
      if (!i)
	wclrtoeol(clt->win->bc);
      if ((nb = special_key(clt, key, str, &i)) == FAILURE)
	wprintw(clt->win->bc, "%c", (str[i++] = (char)key));
      else if (nb == RESET || nb == QUIT)
	return (nb);
      wrefresh(clt->win->bc);
      wrefresh(clt->win->tc);
    }
  if (i >= 40)
    {
      mvwprintw(clt->win->bc, 3, 1, "Name too long, sending it now as is");
      wrefresh(clt->win->bc);
      sleep(1);
    }
  str[i] = '\0';
  return (SUCCESS);
}

static int	get_cmd_arg(t_client *clt)
{
  int		i;
  char		*str;

  i = 0;
  if (!(str = malloc(MAX_LEN + 1)))
    return (FAILURE);
  wattron(clt->win->bc, A_DIM);
  mvwprintw(clt->win->bc, 2, 1, "%s", clt->cmd_arg[clt->cmd]);
  wattroff(clt->win->bc, A_DIM);
  keypad(clt->win->bc, TRUE);
  clt->arg = str;
  return (fill_str(clt, str, i));
}

int	get_cmd(t_client *clt)
{
  int	nb;

  clt->arg = NULL;
  if (!(clt->cmd_arg[clt->cmd]))
    return (SUCCESS);
  else
    {
      put_cmd_bl(clt);
      if ((nb = get_cmd_arg(clt)) != SUCCESS)
	return (nb);
    }
  return (SUCCESS);
}
