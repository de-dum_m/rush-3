/*
** write_all_elements.c for editor in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/src/editor
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 09:28:48 2014 armita_a
** Last update Sun May 11 17:32:23 2014 menigo_m
*/

#include <unistd.h>
#include <fcntl.h>
#include "editor/epic.h"

static int	write_all_champions(t_champ *champions, int fd)
{
  while (champions)
    {
      write_champions(champions, fd);
      champions = champions->next;
    }
  return (0);
}

static int	write_all_monsters(t_monster *monsters, int fd)
{
  while (monsters)
    {
      write_monsters(monsters, fd);
      monsters = monsters->next;
    }
  return (0);
}

static int	write_all_rooms(t_room *rooms, int fd)
{
  while (rooms)
    {
      write_rooms(rooms, fd);
      rooms = rooms->next;
    }
  return (0);
}

int	write_file(t_editor *editor)
{
  int	fd;

  if ((fd = open("game", O_WRONLY | O_CREAT | O_TRUNC,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1)
    {
      my_putstr("error: could not create file\n");
      return (-1);
    }
  write_header(editor, fd);
  write_all_champions(editor->champs, fd);
  write_all_monsters(editor->monsters, fd);
  write_all_rooms(editor->rooms, fd);
  return (0);
}
