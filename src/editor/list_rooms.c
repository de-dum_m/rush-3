/*
** list_rooms.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 01:50:38 2014 chapui_s
** Last update Sun May 11 17:35:02 2014 menigo_m
*/

#include <stdlib.h>
#include "editor/epic.h"

static t_room		*create_room(void)
{
  t_room		*new;

  if ((new = (t_room*)malloc(sizeof(*new))))
    {
      new->name = NULL;
      new->adv = NULL;
      new->connection = NULL;
      new->monster = NULL;
      new->next = NULL;
    }
  else
    my_putstr("error: could not alloc\n");
  return (new);
}

int		push_room(t_editor *editor)
{
  t_room	*tmp;

  if (editor->rooms)
    {
      tmp = editor->rooms;
      while (tmp->next)
	tmp = tmp->next;
      if ((tmp->next = create_room()) == NULL)
	return (-1);
    }
  else
    if ((editor->rooms = create_room()) == NULL)
      return (-1);
  return (0);
}
