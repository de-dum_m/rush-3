/*
** get_elems.c for editor in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/src/editor
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 09:32:56 2014 armita_a
** Last update Sun May 11 17:33:44 2014 menigo_m
*/

#include <unistd.h>
#include "editor/epic.h"
#include "tools/tools.h"

static char	*get_name(char *s)
{
  char		*name;

  name = NULL;
  while (name == NULL || my_strlen(name) == 0)
    {
      my_putstr("Give a name to this ");
      my_putstr(s);
      my_putstr(": ");
      if ((name = get_next_line(0)) == NULL)
	return (NULL);
    }
  return (name);
}

char	*get_str(char *s)
{
  char	*name;

  name = NULL;
  while (name == NULL || my_strlen(name) == 0)
    {
      my_putstr("Give the ");
      my_putstr(s);
      my_putstr(": ");
      if ((name = get_next_line(0)) == NULL)
	return (NULL);
    }
  return (name);
}

int	get_room(t_room *room)
{
  if (((room->name = get_name("room")) == NULL)
      || ((room->adv = get_str("advantage")) == NULL)
      || ((room->connection = get_str("connections name")) == NULL)
      || ((room->monster = get_str("monster name in this room")) == NULL))
    return (-1);
  return (0);
}

int	get_champ(t_champ *champ)
{
  if (((champ->name = get_name("champion")) == NULL)
      || ((champ->type = get_str("type")) == NULL)
      || ((champ->hp = get_str("hp")) == NULL)
      || ((champ->spe = get_str("spe")) == NULL)
      || ((champ->speed = get_str("speed")) == NULL)
      || ((champ->deg = get_str("deg")) == NULL)
      || ((champ->weapon = get_str("weapon")) == NULL)
      || ((champ->armor = get_str("armor")) == NULL))
    return (-1);
  return (0);
}

int	get_monster(t_monster *monster)
{
  if (((monster->type = get_str("type")) == NULL)
      || ((monster->hp = get_str("hp")) == NULL)
      || ((monster->spe = get_str("spe")) == NULL)
      || ((monster->speed = get_str("speed")) == NULL)
      || ((monster->deg = get_str("deg")) == NULL)
      || ((monster->weapon = get_str("weapon")) == NULL)
      || ((monster->armor = get_str("armor")) == NULL))
    return (-1);
  return (0);
}
