/*
** write.c for editor in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/src/editor
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 09:24:11 2014 armita_a
** Last update Sun May 11 17:31:31 2014 menigo_m
*/

#include <unistd.h>
#include "editor/epic.h"
#include "tools/tools.h"

static int	write_str(char *s, int fd)
{
  char		size;

  size = my_strlen(s);
  write(fd, &size, 1);
  my_putstr_fd(s, fd);
  return (0);
}

int	write_header(t_editor *editor, int fd)
{
  write(fd, get_value("MAGIC_NUMBER"), 1);
  write(fd, get_value("HEADER"), 1);
  write(fd, get_value("NAME"), 1);
  write_str(editor->game_name, fd);
  write(fd, get_value("ROOM_TO_WIN"), 1);
  write_str(editor->end, fd);
  write(fd, get_value("ROOM_TO_START"), 1);
  write_str(editor->start, fd);
  write(fd, get_value("SEP_SECTION"), 1);
  return (0);
}

int	write_champions(t_champ *champ_cur, int fd)
{
  write(fd, get_value("CHAMPION"), 1);
  write(fd, get_value("NAME"), 1);
  write_str(champ_cur->name, fd);
  write(fd, get_value("TYPE"), 1);
  write_str(champ_cur->type, fd);
  write(fd, get_value("HP"), 1);
  write_str(champ_cur->hp, fd);
  write(fd, get_value("SPE"), 1);
  write_str(champ_cur->spe, fd);
  write(fd, get_value("SPEED"), 1);
  write_str(champ_cur->speed, fd);
  write(fd, get_value("DEG"), 1);
  write_str(champ_cur->deg, fd);
  write(fd, get_value("WEAPON"), 1);
  write_str(champ_cur->weapon, fd);
  write(fd, get_value("ARMOR"), 1);
  write_str(champ_cur->armor, fd);
  write(fd, get_value("SEP_SECTION"), 1);
  return (0);
}

int	write_monsters(t_monster *monst_cur, int fd)
{
  write(fd, get_value("MONSTER"), 1);
  write(fd, get_value("TYPE"), 1);
  write_str(monst_cur->type, fd);
  write(fd, get_value("HP"), 1);
  write_str(monst_cur->hp, fd);
  write(fd, get_value("SPE"), 1);
  write_str(monst_cur->spe, fd);
  write(fd, get_value("SPEED"), 1);
  write_str(monst_cur->speed, fd);
  write(fd, get_value("DEG"), 1);
  write_str(monst_cur->deg, fd);
  write(fd, get_value("WEAPON"), 1);
  write_str(monst_cur->weapon, fd);
  write(fd, get_value("ARMOR"), 1);
  write_str(monst_cur->armor, fd);
  write(fd, get_value("SEP_SECTION"), 1);
  return (0);
}

int	write_rooms(t_room *room_cur, int fd)
{
  write(fd, get_value("ROOM"), 1);
  write(fd, get_value("NAME"), 1);
  write_str(room_cur->name, fd);
  write(fd, get_value("ADV"), 1);
  write_str(room_cur->adv, fd);
  write(fd, get_value("TAB=>CONNECTION"), 1);
  write_str(room_cur->connection, fd);
  write(fd, get_value("TAB=>MONSTER"), 1);
  write_str(room_cur->monster, fd);
  write(fd, get_value("SEP_SECTION"), 1);
  return (0);
}
