/*
** list_champion.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 04:42:26 2014 chapui_s
** Last update Sun May 11 17:34:50 2014 menigo_m
*/

#include <stdlib.h>
#include "editor/epic.h"

static t_champ		*create_champion(void)
{
  t_champ		*new;

  if ((new = (t_champ*)malloc(sizeof(*new))))
    {
      new->name = NULL;
      new->type = NULL;
      new->hp = NULL;
      new->spe = NULL;
      new->speed = NULL;
      new->deg = NULL;
      new->weapon = NULL;
      new->armor = NULL;
      new->next = NULL;
    }
  else
    my_putstr("error: could not alloc\n");
  return (new);
}

int		push_champ(t_editor *editor)
{
  t_champ	*tmp;

  if (editor->champs)
    {
      tmp = editor->champs;
      while (tmp->next)
	tmp = tmp->next;
      if ((tmp->next = create_champion()) == NULL)
	return (-1);
    }
  else
    if ((editor->champs = create_champion()) == NULL)
      return (-1);
  return (0);
}
