/*
** main.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 00:56:27 2014 chapui_s
** Last update Sun May 11 17:57:36 2014 menigo_m
*/

#include <unistd.h>
#include "editor/epic.h"
#include "tools/tools.h"

static t_info	g_info[] =
{
  {"NAME", 0x01, STRING, 1},
  {"ROOM_TO_WIN", 0x02, STRING, 1},
  {"ROOM_TO_START", 0x03, STRING, 1},
  {"TYPE", 0x04, STRING, 1},
  {"HP", 0x05, INTEGER, 1},
  {"SPEED", 0x06, INTEGER, 1},
  {"DEG", 0x07, INTEGER, 1},
  {"WEAPON", 0x08, STRING, 1},
  {"ARMOR", 0x09, STRING, 1},
  {"ADV", 0x10, STRING, 1},
  {"TAB=>CONNECTION", 0x11, STRING, 1},
  {"TAB=>MONSTER", 0x12, STRING, 1},
  {"ROOM", 0x0F, STRING, 1},
  {"SPE", 0x20, STRING, 1},
  {"CHAMPION", 0x0C, 0, 0},
  {"HEADER", 0x0D, 0, 0},
  {"MONSTER", 0x0E, 0, 0},
  {"SEP_SECTION", 0x0A, 0, 0},
  {"MAGIC_NUMBER", 123, 0, 0},
  {NULL, 0, 0, 0},
};

int	is_alpha(char *s)
{
  int		i;

  i = 0;
  while (s && s[i])
    {
      if (s[i] < '0' || s[i] > '9')
	return (1);
      i += 1;
    }
  if (s == NULL)
    return (1);
  return (0);
}

int			get_nb(char *str)
{
  unsigned int		nb;
  char			*tmp;

  tmp = NULL;
  while (tmp == NULL || is_alpha(tmp))
    {
      my_putstr("Give the numbers of ");
      my_putstr(str);
      my_putstr(": ");
      if ((tmp = get_next_line(0)) == NULL)
	return (-1);
    }
  nb = my_uatoi(tmp);
  return (nb);
}

char			*get_value(char *s)
{
  int			i;

  i = 0;
  while (g_info[i].desc && my_strcmp(g_info[i].desc, s) != 0)
    i += 1;
  return (&(g_info[i].value));
}

int			main(void)
{
  static t_editor	editor;

  if ((get_game(&editor)) == -1)
    return (-1);
  if ((write_file(&editor)) == -1)
    return (-1);
  return (0);
}
