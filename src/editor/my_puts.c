/*
** my_puts.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 01:15:20 2014 chapui_s
** Last update Sun May 11 17:18:52 2014 menigo_m
*/

#include <unistd.h>

void		my_putchar(char c)
{
  write(1, &c, 1);
}

int		my_putstr(char *s)
{
  while (s && *s)
    write(1, s++, 1);
  return (0);
}

int		my_putstr_fd(char *s, int fd)
{
  while (s && *s)
    write(fd, s++, 1);
  return (0);
}

void		my_putnbr(int n)
{
  if (n < 0)
    {
      my_putchar('-');
      n = -n;
    }
  if (n >= 10)
    my_putnbr(n / 10);
  my_putchar((n % 10) + '0');
}
