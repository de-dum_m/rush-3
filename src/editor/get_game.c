/*
** get_game.c for editor in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/src/editor
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 09:39:42 2014 armita_a
** Last update Sun May 11 17:34:30 2014 menigo_m
*/

#include <unistd.h>
#include "editor/epic.h"
#include "tools/tools.h"

static int	get_game_name(t_editor *editor)
{
  char		*name;

  name = NULL;
  while (name == NULL || my_strlen(name) == 0)
    {
      my_putstr("Give a name to the game: ");
      if ((name = get_next_line(0)) == NULL)
	return (-1);
    }
  editor->game_name = name;
  return (0);
}

static int	get_start_end(t_editor *editor)
{
  if (((editor->start = get_str("room start")) == NULL)
      || ((editor->end = get_str("room end")) == NULL))
    return (-1);
  return (0);
}

int	get_game(t_editor *editor)
{
  if ((get_game_name(editor)) == -1
      || get_info_nb(editor) == -1
      || get_info_rooms(editor) == -1
      || get_info_champions(editor) == -1
      || get_info_monsters(editor) == -1
      || get_start_end(editor) == -1)
    return (-1);
  return (0);
}
