/*
** list_monster.c for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 04:57:07 2014 chapui_s
** Last update Sun May 11 17:33:01 2014 menigo_m
*/

#include <stdlib.h>
#include "editor/epic.h"

static t_monster	*create_monster(void)
{
  t_monster		*new;

  if ((new = (t_monster*)malloc(sizeof(*new))))
  {
    new->type = NULL;
    new->hp = 0;
    new->spe = 0;
    new->speed = 0;
    new->deg = 0;
    new->weapon = NULL;
    new->armor = NULL;
    new->next = NULL;
  }
  else
    my_putstr("error: could not alloc\n");
  return (new);
}

int		push_monster(t_editor *editor)
{
  t_monster	*tmp;

  if (editor->monsters)
  {
    tmp = editor->monsters;
    while (tmp->next)
      tmp = tmp->next;
    if ((tmp->next = create_monster()) == NULL)
      return (-1);
  }
  else
    if ((editor->monsters = create_monster()) == NULL)
      return (-1);
  return (0);
}
