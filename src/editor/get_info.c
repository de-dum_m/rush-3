/*
** get_info.c for editor in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/src/editor
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 09:36:04 2014 armita_a
** Last update Sun May 11 09:38:29 2014 armita_a
*/

#include "editor/epic.h"

int	get_info_nb(t_editor *editor)
{
  editor->nb_rooms = get_nb("rooms");
  editor->nb_champs = get_nb("champions");
  editor->nb_monsters = get_nb("monsters");
  return (0);
}

int		get_info_rooms(t_editor *editor)
{
  int		i;
  t_room	*tmp;

  i = 0;
  while (i < editor->nb_rooms)
    {
      my_putstr("\tROOM ");
      my_putnbr(i + 1);
      my_putstr("\n");
      if ((push_room(editor)) == -1)
	return (-1);
      tmp = editor->rooms;
      while (tmp->next)
	tmp = tmp->next;
      get_room(tmp);
      i += 1;
    }
  return (0);
}

int		get_info_champions(t_editor *editor)
{
  int		i;
  t_champ	*tmp;

  i = 0;
  while (i < editor->nb_champs)
    {
      my_putstr("\tCHAMPION ");
      my_putnbr(i + 1);
      my_putstr("\n");
      if ((push_champ(editor)) == -1)
	return (-1);
      tmp = editor->champs;
      while (tmp->next)
	tmp = tmp->next;
      get_champ(tmp);
      i += 1;
    }
  return (0);
}

int		get_info_monsters(t_editor *editor)
{
  int		i;
  t_monster	*tmp;

  i = 0;
  while (i < editor->nb_monsters)
    {
      my_putstr("\tMONSTER ");
      my_putnbr(i + 1);
      my_putstr("\n");
      if ((push_monster(editor)) == -1)
	return (-1);
      tmp = editor->monsters;
      while (tmp->next)
	tmp = tmp->next;
      get_monster(tmp);
      i += 1;
    }
  return (0);
}
