##
## Makefile for rush-3 in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3
## 
## Made by armita_a
## Login   <armita_a@epitech.net>
## 
## Started on  Fri May  9 20:13:21 2014 armita_a
## Last update Sun May 11 18:42:32 2014 armita_a
##

NAME_S	= js_server

NAME_C	= fantasy_client

NAME_E	= epic_editor

SRC_S	= src/server/main.c 			\
	src/tools/my_strlen.c			\
	src/server/fill_header.c		\
	src/server/fill_champion.c		\
	src/server/fill_monster.c		\
	src/server/list_monster.c		\
	src/server/my_put_error.c		\
	src/server/get_nbr_value.c		\
	src/server/list_team.c			\
	src/server/nb_to_char.c			\
	src/server/fill_room_monster.c		\
	src/server/fill_room.c			\
	src/server/fill_connection.c

SRC_C	= src/client/main.c			\
	src/client/gui.c			\
	src/client/init_gui.c			\
	src/client/left_menu.c			\
	src/client/push_cmd.c			\
	src/client/get_cmd.c			\
	src/client/center_log.c			\
	src/client/connect.c			\
	src/tools/my_atoi.c			\
	src/tools/get_next_line.c		\
	src/tools/my_strlen.c

SRC_E	= src/editor/main.c			\
	src/editor/write.c			\
	src/editor/write_all_elements.c		\
	src/editor/get_elems.c			\
	src/editor/get_info.c			\
	src/editor/my_puts.c			\
	src/editor/get_game.c			\
	src/editor/list_rooms.c			\
	src/editor/list_monster.c		\
	src/editor/list_champion.c		\
	src/tools/get_next_line.c		\
	src/tools/my_strlen.c			\
	src/tools/my_atoi.c			\
	src/tools/my_strcmp.c

OBJ_S	= $(SRC_S:.c=.o)

OBJ_C	= $(SRC_C:.c=.o)

OBJ_E	= $(SRC_E:.c=.o)

CFLAGS	= -Wall -Wextra -W -pedantic -Iincludes -g

all:	$(NAME_S) $(NAME_E) $(NAME_C)

$(NAME_S): $(OBJ_S)
	$(CC) $(OBJ_S) -o $(NAME_S)
	@echo -e "\033[1;036m$(NAME_S): \033[0;036mCompiled successfully\033[0m"

$(NAME_C): $(OBJ_C)
	$(CC) $(OBJ_C) -o $(NAME_C) -lncurses
	@echo -e "\033[1;036m$(NAME_C): \033[0;036mCompiled successfully\033[0m"

$(NAME_E): $(OBJ_E)
	$(CC) $(OBJ_E) -o $(NAME_E)  -Iincludes/editor -Iincludes/tools 
	@echo -e "\033[1;036m$(NAME_E): \033[0;036mCompiled successfully\033[0m"

clean:
	rm -f $(OBJ_S) $(OBJ_C)$(OBJ_E)

fclean:	clean
	rm -f $(NAME_S) $(NAME_C) $(NAME_E)

re:	fclean all

.PHONY:	all re fclean clean
