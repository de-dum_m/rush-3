/*
** server.h for epic js fantasy in /home/menigo_m/rendu/rush-3/src/server
**
** Made by menigo_m
** Login   <menigo_m@epitech.net>
**
** Started on  Sat May 10 10:20:31 2014 menigo_m
** Last update Sun May 11 15:17:54 2014 lopes_n
*/

#ifndef SERVER_H_
# define SERVER_H_

# define NAME		0x01
# define ROOM_TO_WIN	0x02
# define ROOM_TO_START	0x03
# define TYPE		0x04
# define HP		0x05
# define SPEED		0x06
# define DEG		0x07
# define WEAPON		0x08
# define ARMOR		0x09
# define ADV		0x10
# define CONNECTION_TAB	0x11
# define MONSTER_TAB	0x12
# define SPE		0x20
# define SEP_SECTION	0x0A
# define CHAMPION	0x0C
# define HEADER		0x0D
# define MONSTER	0x0E
# define ROOM		0x0F
# define MAGIC_NUMBER	123

typedef struct		s_room
{
  char			*name;
  char			*adv;
  char			**connections;
  int			nb_connections;
  int			*monster_id;
  int			nb_monsters;
  struct s_room		*next;
}			t_room;

typedef struct		s_champ
{
  char			*name;
  char			*type;
  char			*weapon;
  int			hp;
  int			spe;
  int			speed;
  int			deg;
  int			armor;
  struct s_champ	*next;
}			t_champ;

typedef struct		s_monster
{
  int			id;
  char			*type;
  char			*weapon;
  int			hp;
  int			spe;
  int			speed;
  int			deg;
  int			armor;
  struct s_monster	*next;
}			t_monster;

typedef struct		s_editor
{
  char			*game_name;
  char			*start;
  char			*end;
  struct s_room		*rooms;
  struct s_champ	*champs;
  struct s_monster	*monsters;
}			t_editor;

typedef struct		s_section
{
  char			name;
  int			(*func)(t_editor *datas, int fd);
}			t_section;

typedef struct		s_attr
{
  char			code;
  void			*name;
}			t_attr;

int	fill_champion(t_editor *datas, int fd);
int	fill_header(t_editor *datas, int fd);
int	fill_monster(t_editor *datas, int fd);
int	fill_room(t_editor *datas, int fd);
int	fill_room_monster(t_editor *datas, char *monster, t_room *room);
char	*get_value(int fd);
int	get_nbr_value(void *ptr, int fd);
int	get_header_name(t_editor *datas, int fd);
int	get_header_start(t_editor *datas, int fd);
int	get_header_win(t_editor *datas, int fd);
int	my_put_error(char *str);
int	my_strlen(char *s);
int	my_elem_len(char *str);
char	*nb_to_char(int nb);
int	exist_connection(t_editor *datas, t_room *tmp);
char	*list_team(t_editor *datas, int value);
char	*list_monster(t_editor *datas, int value);
int	fill_room_connections(t_editor *datas, char *connection, t_room *room);
char	*list_monster(t_editor *datas, int value);

#endif /* !SERVER_H_ */
