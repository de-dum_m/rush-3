/*
** epic.h for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 00:59:16 2014 chapui_s
** Last update Sun May 11 18:41:47 2014 armita_a
*/

#ifndef EPIC_H_
# define EPIC_H_

#include "tools/get_next_line.h"

typedef struct		s_room
{
  char			*name;
  char			*adv;
  char			*connection;
  char			*monster;
  struct s_room		*next;
}			t_room;

typedef struct		s_champ
{
  char			*name;
  char			*type;
  char			*hp;
  char			*spe;
  char			*speed;
  char			*deg;
  char			*weapon;
  char			*armor;
  struct s_champ	*next;
}			t_champ;

typedef struct		s_monster
{
  char			*type;
  char			*hp;
  char			*spe;
  char			*speed;
  char			*deg;
  char			*weapon;
  char			*armor;
  struct s_monster	*next;
}			t_monster;

typedef struct		s_editor
{
  char			*game_name;
  char			*start;
  char			*end;
  int			nb_rooms;
  int			nb_champs;
  int			nb_monsters;
  struct s_room		*rooms;
  struct s_champ	*champs;
  struct s_monster	*monsters;
}			t_editor;

typedef struct		s_info
{
  char			*desc;
  char			value;
  int			type;
  int			nb_params;
}			t_info;

# define STRING		(1)
# define INTEGER	(2)

/*
** get_elems.c
*/
char	*get_str(char *s);
int	get_champ(t_champ *champ);
int	get_monster(t_monster *monster);
int	get_room(t_room *room);

/*
** get_game.c
*/
int	get_game(t_editor *editor);

/*
** get_info.c
*/
int	get_info_champions(t_editor *editor);
int	get_info_monsters(t_editor *editor);
int	get_info_nb(t_editor *editor);
int	get_info_rooms(t_editor *editor);

/*
** list_champion.c
*/
int	push_champ(t_editor *editor);

/*
** list_monster.c
*/
int		push_monster(t_editor *editor);

/*
** list_rooms.c
*/
int	push_room(t_editor *editor);

/*
** main.c
*/
char	*get_value(char *s);
int	get_nb(char *str);
int	is_alpha(char *s);

/*
** my_puts.c
*/
int	my_putstr(char *s);
int	my_putstr_fd(char *s, int fd);
void	my_putchar(char c);
void	my_putnbr(int n);

/*
** write_all_elements.c
*/
int	write_file(t_editor *editor);

/*
** write.c
*/
int	write_champions(t_champ *champ_cur, int fd);
int	write_header(t_editor *editor, int fd);
int	write_monsters(t_monster *monst_cur, int fd);
int	write_rooms(t_room *room_cur, int fd);

#endif /* !EPIC_H_ */
