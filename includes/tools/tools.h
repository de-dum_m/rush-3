/*
** tools.h for tools in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/includes/tools
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 10 09:06:53 2014 armita_a
** Last update Sun May 11 17:59:37 2014 menigo_m
*/

#ifndef TOOLS_H_
# define TOOLS_H_

/*
** my_atoi.c
*/
int		my_atoi(char *str);

/*
** my_strcmp.c
*/
int	my_strcmp(char *s1, char *s2);

/*
** my_strlen.c
*/
int	my_strlen(char *s);

#endif /* !TOOLS_H_ */
