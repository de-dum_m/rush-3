/*
** get_next_line.h for rush in /home/chapui_s/travaux/rush
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 01:18:35 2014 chapui_s
** Last update Sun May 11 18:06:04 2014 armita_a
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define BUF_SIZE	42

typedef struct	s_fd
{
  char		*str;
  int		fd;
  struct s_fd	*next;
}		t_fd;

/*
** get_next_line.c
*/
char	*get_next_line(const int fd);

#endif /* !GET_NEXT_LINE_H_ */
