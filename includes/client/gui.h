/*
** gui.h for client in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3/includes/client
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 10 14:03:20 2014 armita_a
** Last update Sun May 11 17:38:44 2014 menigo_m
*/

#ifndef GUI_H_
# define GUI_H_

# include "client/client.h"

# define BAR_SIZE	3
# define COL_SIZE	20
# define INFO_SIZE	(COLS / 4)

# define VBOR		'|'
# define HBOR		'-'

# define LOGO0		"     _"
# define LOGO1		"    | |___"
# define LOGO2		" _  | / __|"
# define LOGO3		"| |_| \\__ \\"
# define LOGO4		" \\___/|___/"

#endif /* !GUI_H_ */
