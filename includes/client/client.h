/*
** client.h for client in /home/de-dum_m/code/B2-C-Prog_Elem/rush-3
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri May  9 22:49:46 2014 de-dum_m
** Last update Sun May 11 17:32:18 2014 armita_a
*/

#ifndef CLIENT_H_
# define CLIENT_H_

# include <curses.h>
# include "editor/epic.h"

# define FAILURE	-1
# define SUCCESS	1
# define RESET		-2
# define QUIT		-3

# define MAX_LEN	256

# define JS_NEXT	0
# define JS_LIST_T	1
# define JS_LIST_M	2
# define JS_ATTACK	3
# define JS_ATTACK_SPE	4
# define JS_WHO		5
# define JS_NOEL	6

# define JS_NNEXT	"next"
# define JS_NLIST_T	"list_team"
# define JS_NLIST_M	"list_monster"
# define JS_NATTACK	"attack"
# define JS_NATTACK_SPE	"attack_spe"
# define JS_NWHO	"who"
# define JS_NNOEL	"noel"

# define KEY_ESC	27

typedef struct	s_win
{
  WINDOW	*bl;
  WINDOW	*bc;
  WINDOW	*br;
  WINDOW	*tl;
  WINDOW	*tc;
  WINDOW	*tr;
  WINDOW	*ml;
}		t_win;

typedef struct	s_client
{
  int		portnum;
  char		*addr;
  int		sock_fd;
  int		cmd;
  char		*arg;
  char		*cmds[7];
  char		*cmd_arg[7];
  t_win		*win;
  t_editor	*game;
}		t_client;

/*
** center_log.c
*/
void	init_center_log(t_win *win);

/*
** get_cmd.c
*/
int	get_cmd(t_client *clt);

/*
** gui.c
*/
int	start(t_client *clt);
void	refresh_allwin(t_win *win);
void	usage(WINDOW *tc);

/*
** init_gui.c
*/
int	init_gui(t_win *win, t_client *clt);

/*
** left_menu.c
*/
int	left_menu(WINDOW *tl, t_client *clt);

/*
** main.c
*/
int	size_term();

/*
** push_cmd.c
*/
int	push_cmd(t_client *clt);

/*
** connect.c
*/
int	client_connect(char *addr, int port, t_client *clt);

#endif /* !CLIENT_H_ */
