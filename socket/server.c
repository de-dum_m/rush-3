/*
** main.c for rush in /home/chapui_s/test/server_client
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 10 20:58:28 2014 chapui_s
** Last update Sun May 11 17:02:50 2014 armita_a
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define CLIENTS_MAX		(255)
#define BUF_SIZE		(1024)

typedef struct		s_client
{
  int			fd;
  int			closed;
}			t_client;

typedef struct		s_info_connect
{
  struct sockaddr_in	server;
  struct sockaddr_in	sock_tmp;
  t_client		clients[CLIENTS_MAX];
  fd_set		rdfs;
  int			fd_server;
  int			nb_clients;
  int			fd_max;
  int			fd_tmp;
  int			port;
  int			size_tmp;
}			t_info_connect;

typedef struct		s_function
{
  char			id_fct;
  char			empty;
  char			*(*function)(char *param);
}			t_function;

t_function		tab_fct[] =
{
  {0, 0, NULL},
};

int			my_strcmp(char *s1, char *s2)
{
  return ((*s1 == *s2 && *s1) ? (my_strcmp(++s1, ++s2)) : (*s1 - *s2));
}

int			my_strlen(char *s)
{
  int			size;

  size = 0;
  while (s && s[size])
    size += 1;
  return (size);
}

int			my_putstr(char *s)
{
  while (s && *s)
    write(1, s++, 1);
  return (-1);
}

int			init_connect(t_info_connect *info)
{
  if ((info->fd_server = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return (my_putstr("error: socket\n"));
  info->server.sin_family = AF_INET;
  info->server.sin_addr.s_addr = INADDR_ANY;
  info->server.sin_port = htons(info->port);

  if ((bind(info->fd_server, (struct sockaddr*)&(info->server),
	    sizeof((info->server)))) == -1)
    return (my_putstr("error: bind\n"));
  if ((listen(info->fd_server, CLIENTS_MAX)) == -1)
    return (my_putstr("error: listen\n"));
  info->fd_max = info->fd_server;
  return (0);
}

int			is_exit(void)
{
  char			buffer[BUF_SIZE];
  int			size;

  if ((size = read(0, buffer, BUF_SIZE)) == -1)
    return (my_putstr("error: read\n"));
  buffer[size] = 0;
  if (size > 0)
   buffer[size - 1] = 0;
  if (my_strcmp(buffer, "exit") == 0)
    return (1);
  return (0);
}

int			add_client(t_info_connect *info)
{
  struct sockaddr_in	sock_tmp;
  int			fd_tmp;
  size_t		size;
  char			*to_return;

  my_putstr("New client !\n");
  size = sizeof(sock_tmp);
  if ((fd_tmp = (accept(info->fd_server, (struct sockaddr*)&(sock_tmp),
			(socklen_t*)&(size)))) == -1)
    return (my_putstr("error: accept\n"));
  info->fd_max = (fd_tmp > info->fd_max) ? (fd_tmp) : (info->fd_max);
  FD_SET(fd_tmp, &(info->rdfs));
  info->clients[info->nb_clients].fd = fd_tmp;
  info->clients[info->nb_clients].closed = 0;
  int i = info->nb_clients;
  to_return = "Bienvenue sur le server";
  write(info->clients[i].fd, to_return, my_strlen(to_return));
  sleep(5);
  to_return = "OK NB:1 TURN";
  write(info->clients[i].fd, to_return, my_strlen(to_return));
  info->nb_clients += 1;
  return (0);
}

char			*exec_cmd(char *buffer, int v)
{
  int			i;

  i = 0;
  if (buffer[0] != 127)
    printf("client[%d]: '%c' '%s'\n", v, buffer[0], buffer + 2);
  while (tab_fct[i].function && buffer[0] != tab_fct[i].id_fct)
    i += 1;
  if (tab_fct[i].function)
  {
    return (tab_fct[i].function(buffer + 2));
  }
  return (buffer - 2);
  /* return (NULL); */
}

int			manage_clients(t_info_connect *info)
{
  char			buffer[BUF_SIZE];
  char			*to_return;
  int			size;
  int			i;

  i = 0;
  while (i < info->nb_clients)
  {
    if (FD_ISSET(info->clients[i].fd, &(info->rdfs)))
      {
	if ((size = read(info->clients[i].fd, buffer, BUF_SIZE)) < 0)
	  {
	    close(info->clients[i].fd);
	    FD_CLR(info->clients[i].fd, &(info->rdfs));
	    if (info->nb_clients > 1)
	      info->clients[i].fd = info->clients[info->nb_clients].fd;
	    info->nb_clients -= 1;
	  }
	else
	  {
	    buffer[size] = 0;
	    exec_cmd(buffer, i);
       	    to_return = "reponse    TURN";
	    write(info->clients[i].fd, to_return, my_strlen(to_return));
	    sleep(1);
	    write(info->clients[i].fd, to_return, my_strlen(to_return));
	    if (buffer[0] == 127)
	      {
		printf("client exited\n");
		close(info->clients[i].fd);
		FD_CLR(info->clients[i].fd, &(info->rdfs));
		if (info->nb_clients > 1)
		  info->clients[i].fd = info->clients[info->nb_clients].fd;
		info->nb_clients--;
	      }
	  }
      }
    i++;
  }
  return (0);
}

int			main(int argc, char **argv)
{
  t_info_connect	info;
  int			i;

  if (argc == 1)
    return (0);
  info.port = atoi(argv[1]);
  printf("port = %d\n", info.port);
  info.nb_clients = 0;
  if ((init_connect(&(info))) == -1)
    return (-1);
  while (1)
  {
    i = 0;
    FD_ZERO(&(info.rdfs));
    FD_SET(0, &(info.rdfs));
    FD_SET(info.fd_server, &(info.rdfs));
    while (i < info.nb_clients)
    {
      FD_SET(info.clients[i].fd, &(info.rdfs));
      i += 1;
    }
    if ((select(info.fd_max + 1, &(info.rdfs), NULL, NULL, NULL)) == -1)
      return (my_putstr("error: select\n"));
    if ((FD_ISSET(0, &(info.rdfs))))
    {
      if ((is_exit()) == 1)
	break ;
    }
    else if (FD_ISSET(info.fd_server, &(info.rdfs)))
    {
      if ((add_client(&info)) == -1)
	return (-1);
    }
    else
    {
      if ((manage_clients(&info)) == -1)
	return (-1);
    }
  }
  close(info.fd_server);
  return (0);
}
