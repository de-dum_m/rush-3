/*
** client.c for socket in /home/armita_a/Documents/Teck_1/Prog_elem/rush-3
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 11 09:57:25 2014 armita_a
** Last update Sun May 11 10:46:32 2014 de-dum_m
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>

int			my_putstr(char *s)
{
  while (s && *s)
    write(1, s++, 1);
  return (0);
}

int			client_send(char **argv)
{
  int			fd;
  struct sockaddr_in	server;
  char			buf[2000];

  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return (my_putstr("error: socket\n"));
  server.sin_addr.s_addr = inet_addr(argv[1]);
  server.sin_family = AF_INET;
  server.sin_port = htons(atoi(argv[2]));
  if ((connect(fd, (struct sockaddr*)&server, sizeof(server))) == -1)
    return (my_putstr("error: connect\n"));
  while ((write(fd, argv[3], strlen(argv[3]))) != -1)
    usleep(1000);
  /* if ((read(fd, buf, 2000)) == -1) */
  /*   return (my_putstr("error: read\n")); */
  /* puts(buf); */
  close(fd);
  return (0);
}

int	main(int ac, char **av)
{
  if (ac > 3)
    client_send(av);
  else
    printf("Apprends a compter\n");
  return (0);
}
